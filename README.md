Gnome Shell 插件：Gnome全局菜单 v0.8.1-Alpha
--------------

本插件仅提供英文与简体中文支持（其中文档仅支持简体中文），其他语言用户请访问[原版](https://gitlab.com/lestcape/Gnome-Global-AppMenu)。
This extension supports only English and Chinese, users of other languages can visit [original project](https://gitlab.com/lestcape/Gnome-Global-AppMenu) for a better experience.

最近更新：2018年6月12日


特别鸣谢：
--------------

- ![@rgcjonas](https://github.com/rgcjonas)                  初始代码是他写的。
- ![@jaszhix](https://github.com/jaszhix)                    帮助从python移植到gjs。
- ![@mtwebster](https://github.com/mtwebster)                帮助在Cinnamon桌面上实现。
- ![@collinss](https://github.com/collinss)                  修复firefox和thunderbird上出现的一些问题。
- ![@rilian-la-te](https://gitlub.com/rilian-la-te)          修复诸多问题。
- ![@lestcape](https://gitlab.com/lestcape)                  完成Gnome-Global-AppMenu插件并进行维护。
- ![Ubuntu devs](https://github.com/ubuntu/)                 提供协议与修补。
- ![Cinnamon devs](https://github.com/linuxmint/cinnamon)    编写了最初的菜单，尤其感谢 ![@JosephMcc](https://github.com/JosephMcc/)的帮助。
- ![Gnome devs](https://gitlub.com/GNOME/gnome-shell)        提供内部API和工具支持。

![](https://gitlab.com/shulinbao/Gnome-Global-Menu/raw/master/GnomeGlobalMenu@shulinbao/Capture.png)

关于Wayland支持:
--------------
本插件支持Wayland，但可能会出现以下问题：
- gnome-terminal的菜单可能无法被正确支持。
- 对于非Gtk窗口，菜单可能无法正确地工作。

其他的已知问题:
--------------
- 并非所有的程序都经过了我们的测试，当然如果在使用中如果出现了问题，欢迎向我们反馈。
- 像Blender这类有自己GUI库的程序可能无法支持。
- Gnome全家桶里的某些程序（例如Nautilus）现在已经不支持全局菜单了。如果你非常想用全局菜单的话，请使用别的程序以代替。

更新日志
--------------
0.8.1-Alpha
从[原版](https://gitlab.com/lestcape/Gnome-Global-AppMenu)派生而来
- 添加更多中文支持
- 移除可能导致不稳定的实验项目
- 重写了README

安装向导:
--------------
1. 安装unity-gtk-module。
  * sudo apt-get install unity-gtk2-module unity-gtk3-module
2. 重启你的电脑。
3. 在https://gitlab.com/shulinbao/Gnome-Global-AppMenu下载插件。
4. 将gnomeGlobalAppMenu@shulinbao文件夹解压至~/.local/share/gnome-shell/extensions/
5. 在Gnome Tweak Tool中启用本插件。
6. 注销并重新登录你的用户。

如何卸载:
--------------
1. 停用本插件。
2. 重设以下gsettings values:
  * gsettings reset org.gnome.settings-daemon.plugins.xsettings overrides
  * gsettings reset org.gnome.settings-daemon.plugins.xsettings enabled-gtk-modules
3. 如果其他桌面环境不需要unity-gtk-module依赖，那么你可以卸载掉先前安装的unity-gtk-module
重启你的电脑。

本软件是自由软件:
--------------
- 本文档是自由软件；您可以在自由软件基金会发布的 GNU 公共许可证的条款下重新发布或修改它；您应当使用该许可证的第二版本。
- 本程序发布的目的是希望它对您有用，但没有任何担保，即使针对商业或其它特定应用目的。请查阅 GNU 公共许可证以获得更多细节。
- 您应当在收到本程序的同时也收到了一份 GNU 公共许可证的副本；如果没有收到，请给自由软件基金会写信。地址是：51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
- 如果您对自由软件感兴趣，欢迎您阅读[这样一份参考材料](https://fsfs-zh.readthedocs.io/zh/latest/)
